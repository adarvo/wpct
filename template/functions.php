<?php

include_once "_template/lib.php";

import_safe("assets.config.php");
import_safe("_template/asset-handler.php");
import_safe("_template/install-plugins.php");

$packageData = require_json('./package.json');
$projectTextDomain = $packageData['template-core-config']['wp-config']['text-domain'];

/**
 * ACF init 
 */
function theme__acf_init() {
	
  /* Set Google Maps API Key */
  acf_update_setting('google_api_key', 'AIzaSyB3mymhSmWQCuGlLKUocSsAt5-8_VIaJBk'); 
 
/* Creating ACF options page */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> __('Theme Setup', $projectTextDomain),
		'menu_title'	=> __('Theme Setup', $projectTextDomain),
		'menu_slug' 	=> 'theme-setup',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));	
}

if( function_exists('acf_register_block') ) {
		
  // registra o bloco de equipe
  acf_register_block(array(
    'name'				=> 'cta',
    'title'				=> __('CTA'),
    'description'		=> __('Block to render a CTA section.'),
    'render_callback'	=> 'render_blocks_callback',
    'category'			=> 'common',
    'mode' => 'preview',
    'icon'				=> 'align-center',
    'keywords'			=> array( 'cta', 'call to action' ),
  ));
}
}
add_action('acf/init', 'theme__acf_init');

/** Callback para criação dos blocos ACF */
function render_blocks_callback( $block ) {
	
	$slug = str_replace('acf/', '', $block['name']);

	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}

/**
 * Setup theme
 */
function setup_theme() {

  /* Adding supports */
  add_post_type_support( 'page', array('excerpt', '') );
  add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

  /* Adding menus */
  register_nav_menus( array(
      'main_menu' => __('Main menu', $projectTextDomain),
      'top_menu' => __('Top menu', $projectTextDomain),
      'footer_menu' => __('Footer menu', $projectTextDomain),
  ) );

  /* Adding custom image sizes */
  add_image_size('post-thumb', 340, 225, true);

  /* Remove auto paragraph option */
  //remove_filter( 'the_content', 'wpautop' );
  
  /* Allow Wordpress to manage page titles */ 
  add_theme_support( 'title-tag' );

}
add_action('init', 'setup_theme');