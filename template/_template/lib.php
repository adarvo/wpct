<?php
  
  function import_safe($filePath) {
    if (file_exists($filePath)) {
      include_once $filePath;
    }
  }