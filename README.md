# Wordpress Create Theme (wp-create-theme)
### Flags
  ```
  *--target-folder
    Define target folder to create template

  --update-only
    Do not overwrite any files, just update

  --skip-npm-install
     Skip npm install step

  * Required
  ```