<?php 
  
  get_header();
  get_template_part('template-parts/header-title-section');
  if(have_posts()):
    while(have_posts()): the_post();
?>

<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-9 col-lg-8 order-2 order-md-1">
					<div class="row mb-5">								
							<div class="col-12">
								<p class="description mb-4">
								<h3><?php _e('Abstract:', 'wpshards');?></h3>
								<?php the_content();?>
								</p>
								<a class="btn btn-primary btn-sm btn-xl" role="button" href="<?php the_field('arquivo_para_download');?>" target="_blank"><?php _e('Download PDF', 'wpshards');?></a>
							</div>
					</div>
				</div>

<!-- Start: sidebar -->
	<?php get_template_part('template-parts/sidebar');?>
<!-- Start: sidebar -->
</div>
</div>

<?php 
  endwhile;
  endif;
  get_footer();
?>