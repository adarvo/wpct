<?php

define(
  'ASSETS_SCRIPT_PATH',
  get_template_directory_uri() . '/js'
);

/* EDIT BELOW THIS LINE */

define(
  "ASSETS_CONFIG",
  array(
    "register" => array(
      "assets" => array(
        array(
          "id" => "theme__font_open_sans",
          "type" => "style",
          "href" => "https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
        ),
        array(
          "id" => "theme__font_awesome",
          "type" => "style",
          "href" => "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
        ),

        array(
          "id" => "bootstrap",
          "type" => "style",
          "href" => "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        ),
        array(
          "id" => "owl",
          "type" => "style",
          "href" => "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css",
        ),
        array(
          "id" => "owl-theme",
          "type" => "style",
          "href" => "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css",
        ),
        array(
          "id" => "main_style",
          "type" => "style",
          "href" => get_stylesheet_uri(),
        ),
        array(
          "id" => "popper",
          "href" => "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
          "deps" => array('jquery'),
          "version", '1.0.0',
        ),
        array(
          "id" => "bootstrap",
          "href" => "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
          "deps" => array('jquery'),
          "version", '1.0.0',
        ),
        array(
          "id" => "owl",
          "href" => "https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js",
          "deps" => array('jquery'),
          "version", '1.0.0',
        ),
        array(
          "id" => "app_script",
          "href" => ASSETS_SCRIPT_PATH . '/app.js',
          "deps" => array('jquery'),
          "version", '1.0.0',
        )
      )
    ),

    "router" => array(
      "general" => array(
        "assets" => array(
          "bootstrap",
          "owl",
          "owl-theme",
          "theme__font_open_sans",
          "theme__font_awesome",
          "popper",
          "bootstrap",
          "owl",
          "app_script"
        )
      )
    )
  )
);
