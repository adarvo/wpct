<header class="mb-5 mt-navbar py-5 title-section">
  <div class="container text-center">
    <h1 class="text-light"><?php the_title(); ?></h1>
    <div class="breadcrumb text-center text-light"><?php get_breadcrumb(); ?></div>
  </div>
</header>