<?php

  function require_json ($filepath) {
    // Read JSON file
    $jsonFile = file_get_contents($filepath);

    //Decode JSON
    return json_decode($jsonFile, true);
  }

  function install_plugins () {
    $packageData = require_json('./package.json');
    $wp_plugins_list = $packageData['template-core-config']['wp-plugins'];

    foreach ($wp_plugins_list as $index => $plugin) {
      $plugin_slug = 'wp-content/plugins/' . $plugin['name'] . '/' . $plugin['main-file'];
      $plugin_zip = $plugin['url'];
      
      if ( !is_plugin_installed( $plugin_slug ) ) {
        $installed = install_plugin( $plugin_zip );
  
        if ( !is_wp_error( $installed ) && $installed ) {
          $activate = activate_plugin( $plugin_slug );
        }
      }
    }
  }
    
  function is_plugin_installed( $slug ) {
    if ( ! function_exists( 'get_plugins' ) ) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $all_plugins = get_plugins();
    
    if ( !empty( $all_plugins[$slug] ) ) {
      return true;
    } else {
      return false;
    }
  }
  
  function install_plugin( $plugin_zip ) {
    include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
    wp_cache_flush();
    
    $upgrader = new Plugin_Upgrader();
    $installed = $upgrader->install( $plugin_zip );
  
    return $installed;
  }
  
  function upgrade_plugin( $plugin_slug ) {
    include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
    wp_cache_flush();
    
    $upgrader = new Plugin_Upgrader();
    $upgraded = $upgrader->upgrade( $plugin_slug );
  
    return $upgraded;
  }

  # define hook
  add_action( 'init', 'install_plugins' );