const projectName = 'fenix-wpcore'

const config = {
  "package": {
    "name": projectName,
    "author": "Agência Fenix"
  },
  "wp-config": {
    "name": "Fenix WP Core Theme",
    "text-domain": projectName
  },

  "wp-plugins": [
    {
      "name": "advanced-custom-fields-pro",
      "main-file": "acf.php",
      "url": "https://connect.advancedcustomfields.com/index.php?a=download&p=pro&k=b3JkZXJfaWQ9OTE0MTd8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTEwLTExIDE0OjA0OjU2"
    }
  ],

  "sftp": {
    "name": projectName,
    "host": "HOST",
    "username": "SSH_USER",
    "password": "SSH_PASSWORD",
    "remotePath": "SSH_REMOTE_PATH",
    "uploadOnSave": false
  }
}


module.exports = config