const args = require('yargs').argv
const package = require('../../package.json')
const readFileReplace = require('../lib/read-file-replace')
const creteStyleHeaderComment = require('../lib/create-style-header-comment')

const themeName = package['template-core']

async function transform (path, _watcherArgs) {
  /* allow to call directly from file as a command */
  const filepath = path || args.filepath;

  await readFileReplace(filepath, {
    find: /\/\*(.|[\r\n])*?\*\//,
    replaceWith: creteStyleHeaderComment({
      version: package.version,
      textDomain: config.domain,
      themeName: themeName
    })
  })

  console.log(`[${Date.now()}] Header for ${filepath} was generated`)
}

module.exports = transform