<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?php echo get_field('favicon', 'options');?>" type="image/x-icon" />
    <?php wp_head(); ?>  
</head>

<body id="page-top" <?php body_class(); ?>>
<nav class="navbar navbar-expand navbar-top" id="topNav">
    <div class="container">
      <p class="d-none d-md-inline pl-4"><?php _e('Request a demonstration: +55 (61) 4042-2813 | <a href="mailto:info@terrapro.io">info@terrapro.io</a>', 'wpshards'); ?></p>
        <?php
        $menu_args = array( 
          'menu' => 'top_menu',
          'theme_location' => 'top_menu',
          'menu_class' => 'nav navbar-nav ml-auto',
          'container_class' => 'collapse navbar-collapse pr-3',
          'container_id'=> 'navbarResponsive',
        );

        wp_nav_menu($menu_args);
        ?>
    </div>
  </nav>

  <nav class="navbar navbar-expand-lg navbar-light bg-light" id="mainNav">
    <div class="container">
      <?php
        $logo = get_field('logotipo', 'options');
      ?>
      <a href="<?php echo bloginfo('url');?>" class="navbar-brand">
        <img class="logo" src="<?php echo $logo['sizes']['medium'];?>"/>
      </a>
      <button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler navbar-toggler-right" type="button" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-align-justify"></i></button>
        <?php
        $menu_args = array( 
          'menu' => 'main_menu',
          'theme_location' => 'main_menu',
          'menu_class' => 'nav navbar-nav ml-auto',
          'container_class' => 'collapse navbar-collapse',
          'container_id'=> 'navbarResponsive',
        );

        wp_nav_menu($menu_args);
        ?>
    </div>
  </nav>