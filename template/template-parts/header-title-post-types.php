<header class="mb-5 mt-navbar py-5 title-section">
  <div class="container text-center">
    <?php
      $post_type_obj = get_post_type_object(get_post_type());
      $title = ($post_type_obj->label == "Posts") ? "Blog" : $post_type_obj->label;
    ?>
    <h1 class="text-light"><?php echo $title; ?></h1>
    <div class="breadcrumb text-center text-light"><?php get_breadcrumb(); ?></div>
  </div>
</header>