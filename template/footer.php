  <footer>
    <div class="p-1 my-4 title-section"></div>
    <div class="col-12">
      <p class="copyright text-center">
        Copyright. Desenvolvido por <a href="https://fenixagencia.com.br" target="_blank" rel="nofollow">FENIX</a>
      </p>
    </div>
  </footer>
    <?php wp_footer(); ?>
  </body>
</html>