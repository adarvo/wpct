const spawn = require('child_process').spawnSync;
const consola = require('consola');

function run (cmd, args = []){
  spawn(cmd, args, { stdio: 'inherit' });
};

module.exports = run