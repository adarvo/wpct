<div class="col-sm-12 col-md-3 col-lg-4 px-4 order-1 order-md-2">
    <div class="row">
        <div class="col" id="col-search-box">
            <h5 class="heading-4"><?php _e('Search', 'wpshards');?></h5>
            <?php get_search_form(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <hr class="my-4 mw-initial gray">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h5 class="heading-4 mb-4"><?php _e('Categories', 'wpshards');?></h5>
            <ul class="list-group categories-filter">
                <?php 
                    $category = (is_post_type_archive('biblioteca') || is_singular('biblioteca') || is_tax()) ? 'library-category' : 'category';
                    $args = array(
                        'title_li' => '',
                        'taxonomy' => $category 
                    );
                    wp_list_categories($args);
                ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <hr class="my-4 mw-initial gray">
        </div>
    </div>
</div>