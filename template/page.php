<?php 
  
  get_header();
  get_template_part('template-parts/header-title-section');
  if(have_posts()):
    while(have_posts()): the_post();
?>

<div class="container">
    <section id="content">
        <div class="row px-3">
            <div class="col">
                <?php the_content();?>
            </div>
        </div>
    </section>
</div>


<?php 
  endwhile;
  endif;
  get_footer();
?>