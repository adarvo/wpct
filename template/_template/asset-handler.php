<?php

  $register = ASSETS_CONFIG['register'];
  $router = ASSETS_CONFIG['router'];

  function get_asset_type ($id, $dict) {
    return $dict[$id]['type'] == 'style' ? 'wp_enqueue_style' : 'wp_enqueue_script';
  }

  function get_assettype_register_fn ($type) {
    return $type === 'style' ? 'wp_register_style' : 'wp_register_script';
  }

  function get_asset_type_enqueue_fn ($type) {
    return $type === 'style' ? 'wp_enqueue_style' : 'wp_enqueue_script';
  }

  /* Enqueue Scripts */
function asset_handler() {
  
  # register phase
  foreach ($register['assets'] as $key => $asset) {
    $assetRegister = get_asset_type_register_fn($asset['type']);
    $assetArgs = array($asset['id'], $asset['href']);

    if ($asset['type'] === 'script') {
     array_push(
       $assetArgs,
       ($asset['deps'] || array()), ($asset['version'] || '1.0.0'), true
      );
    }
    
    call_user_func_array($assetRegister, $assetArgs);
  }

  # include registered assets for general route
  foreach ($router['general']['assets'] as $assetId) {
    $assetType = get_asset_type($assetId, $register['assets']);
    $assetEnqueue = get_asset_type_enqueue_fn($assetType);

    call_user_func_array($assetEnqueue, array($assetId));
  }
}
/* Actions */
add_action( 'wp_enqueue_scripts', 'asset_handler' );