const chokidar = require('chokidar')
const args = require('yargs').argv
const transform = require('./transform')

if (!args.watch) {
  console.log(`[style-header-comment]: Transforming ${args.filepath}`)
  return transform(args.filepath, args)
}

/* Should start watcher */
console.log(`Start watch file ${args.filepath}`)
const watcher = chokidar.watch(args.filepath, { persistent: true });

watcher.on('change', (path, stats) => {
  transform(args.filepath, args, { path, stats })
})