#!/usr/bin/env node

const consola = require('consola')
const args = require('yargs').argv
const fs = require('fs-extra')
const path = require('path')

const rootPath = (...args) => path.resolve(__dirname, ...args)

const writeFile = require('./lib/writeFile')
const copyTemplate = require('./lib/copyTemplate')
const run = require('./lib/run')
const merge = require('deepmerge')

let templatePackage = require('./template/package.json')
const templateConfig = require('./template.config.js')

async function program () {
  const targetFolder = args['target-folder'];
  /* Require targetFolder flag */
  if (!targetFolder) {
    return consola.error('--target-folder flag is required')
  }
  
  const targetSFTPPath = path.join(targetFolder, '.vscode/sftp.json')
  const targetFolderPackagePath = path.join(targetFolder, 'package.json')

  if (!args['update-only']) {
    /* Create target folder if not exists */
    consola.info('Re-creating target folder...')
    fs.removeSync(targetFolder)
  } else {
    consola.info('Updating target folder to latest version')
  }

  copyTemplate(rootPath('./template/'), targetFolder)
  let targetSFTP = require(targetSFTPPath)
  
  //#region Extend package.json
  if (args['update-only']) {
    templatePackage = merge(
      templatePackage,
      /* prefer target package to overwrite defaults */
      require(targetFolderPackagePath) || {}
    )
  }

  /* Remove private info */
  const safeTemplateConfig = merge(templateConfig, { sftp: undefined })

  templatePackage['template-core-config'] = merge(
    templatePackage['template-core-config'] || {},
    safeTemplateConfig
  )

  templatePackage.name = templateConfig.package.name
  templatePackage.author = templateConfig.package.author
  //#endregion

  //#region Create SFTP file from TemplateConfig
  targetSFTP = merge(targetSFTP, templateConfig.sftp)
  //#endregion
  
  consola.info('Updating package.json')
  writeFile(targetFolderPackagePath, JSON.stringify(templatePackage, null, 2))

  consola.info('Updating vscode sftp.json file')
  writeFile(targetSFTPPath, JSON.stringify(targetSFTP, null, 2))

  if (!args['skip-npm-install']) {
    consola.info('Moving into target directory')
    process.chdir(targetFolder)
    consola.info('Installing template dependencies')
    run('npm' , ['--prefix', targetFolder, 'install'])
  } else {
    consola.info('Skipping "npm install" at target folder')
  }

  consola.success('Done')
}

program().catch(consola.error)