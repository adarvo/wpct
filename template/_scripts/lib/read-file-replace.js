const fs = require('fs')

/**
 * Read File and Replace matches
 *
 * @param {string} filepath - File Path to read/replace/write
 * @param {Object} opts - Supoorted Options
 * @param {string} opts.find - Text to find
 * @param {string} opts.replaceWith - Text to reaplace matches
 * @returns
 */
function readFileReplace (filepath, opts) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, 'utf8', function (err,data) {
      if (err) {
        return reject(err);
      }
  
      const result = data.replace(opts.find, opts.replaceWith);
    
      fs.writeFile(filepath, result, 'utf8', function (err) {
         if (err) return reject(err);
         resolve()
      });
    });
  })
}

module.exports = readFileReplace