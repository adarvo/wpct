<?php 
  
  get_header();
  get_template_part('template-parts/header-title-post-types');
?>
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-9 col-lg-8 order-2 order-md-1">
				<?php

				if (have_posts()):
					while (have_posts()): the_post();
					?>
					<div class="row my-5">								
							<div class="col-12">
								<h2 class="name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
								<p class="description mb-4">
								<?php the_excerpt();?>
								</p>
								<a class="btn btn-primary btn-sm btn-xl" role="button" href="<?php the_field('arquivo_para_download');?>" target="_blank"><?php _e('Download PDF', 'wpshards');?></a>
							</div>
					</div>
					<?php endwhile;?>
					<div class="clearfix w-100"></div>
						<!-- Start: pagination -->
						<div class="row mt-5">
							<div class='col'>
								<?php

									$pag_args = array(
										'next_text' => '»',
										'prev_text' => '«',
										'screen_reader_text' => ' '
									);
									the_posts_pagination($pag_args);
									wp_reset_query();
								?>
							</div>
						</div>
						<!-- End: pagination -->
				</div>
		
<?php else: ?>
	<div class="col-lg-12 item item-horizontal">
		<div class="article-content">
			<h2><?php _e('No posts found.', 'wpshards');?></h2>
		</div>
	</div>
<?php endif;?>


<!-- Start: sidebar -->
	<?php get_template_part('template-parts/sidebar');?>
<!-- Start: sidebar -->
</div>
</div>
<?php 
  get_footer();
?>