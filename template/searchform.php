<form action="/" method="get" accept-charset="utf-8" id="searchform" role="search">
  <div>
    <input type="text" name="s" id="s" value="<?php the_search_query(); ?>" placeholder="<?php  _e('What you\'re searching for?', 'wpshards');?>" class="input-rounded mt-2 border-0 w-100"> 
  </div>
</form>