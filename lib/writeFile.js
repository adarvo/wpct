const fs = require('fs')
const promisify = require('util').promisify
const writeSync = promisify(fs.writeFileSync)
const merge = require('deepmerge')

const consola = require('consola')

async function writeFile (filepath, data, opts = {}) {
  opts = merge({ encoding: 'utf8' }, opts)

  consola.info(`Writing file ${filepath}`)
  writeSync(filepath, data, opts)
  consola.info(`File ${filepath} updated`)
}

module.exports = writeFile