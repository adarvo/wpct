
/**
 * Create Style Header Comment
 *
 * @param {Object} opts Supported Options
 * @param {string} opts.themeName - Theme Name
 * @param {string} opts.version - Version (from package)
 * @param {string} opts.textDomain - Text Domain
 * @returns
 */
module.exports = function (opts) {
  const fallbackVersion = '1.0.0'
 
  return `/*
* Theme Name: ${opts.themeName}
* Author: FENIX
* Author URI: https://fenixagencia.com.br
* Version: ${opts.version | fallbackVersion }
* Text Domain: ${opts.textDomain}
*/
`
}