const fs = require('fs-extra')
const consola = require('consola')

const filter = (path) => !(path.indexOf('node_modules') > -1)

async function copyTemplate (fromFolder, toFolder) {
  consola.info('Copying template to target folder')
  fs.copySync(fromFolder, toFolder, { recursive: true, filter })
  consola.info(`Template created at ${toFolder}`)
}

module.exports = copyTemplate