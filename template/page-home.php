<?php 
  /**
   * Template Name: Home
   */
  get_header();
  if(have_posts()):
    while(have_posts()): the_post();
?>
<header class="mt-5 mt-navbar py-5">
  <div class="container text-center ">
    <h1><?php the_title(); ?></h1>
    <a class="btn btn-primary my-4 p-3 text-uppercase" href="#shop" role="button"><i class="fa fa-calculator mr-3"></i><?php _e('See computacional modules', 'wpshards');?></a>
    <hr>
  </div>
</header>
<div class="container">
    <section id="content">
        <div class="row px-3">
            <div class="col">
                <?php the_content();?>
            </div>
        </div>
        <div class="row bg-light">
            <div class="col">
            <h2 class="text-center mt-3"><?php _e('How it works:', 'wpshards');?></h2>
            </div>
        </div>
        <div class="row bg-light">
            <?php 
                /**
                 * Show how it works steps
                 */
                    if(have_rows('how_it_works')):
                    $i = 1;
                    while(have_rows('how_it_works')): the_row();
                ?>
            <div class="col-12 col-md-4 step text-center border border-light bg-white">
            <span class="d-block font-weight-bold py-3"><?php echo $i;?></span>
            <?php echo wp_get_attachment_image(get_sub_field('icon'), 'medium');?>
            <h3 class="mt-3 "><?php the_sub_field('title');?></h3>
            <p class="text-center"><?php the_sub_field('description');?></p>
            </div>
                <?php
                    $i++;
                    endwhile;
                    endif;
                ?>
                
        </div>
        <hr>
        <div class="py-1 bg-light" id="shop">
            <div class="col">
            <h2 class="text-center mt-3"><?php _e('Software store:', 'wpshards');?></h2>
            </div>
        </div>
            <script>
              jQuery(document).ready(function(){

                $args = {
                    autoplay: true,
                    responsive: {
                        0: {
                            items : 1
                        },
                        640: {
                            items: 2
                        }
                    }

                }
                jQuery(".owl-carousel").owlCarousel($args);
              });
            </script>
            <div class="owl-carousel modules bg-light">
            <?php
                $query = new WC_Product_Query();
                $products = $query->get_products();
                foreach($products as $product):
            ?>
                <div class="col py-5 text-center border border-light bg-white">

                    <h3 class="py-4"><?php echo $product->name;?></h3>
                    <?php echo wc_price($product->price);?>
                    <p><?php echo $product->short_description;?></p>
                    <a class="btn btn-primary" href="<?php the_permalink($product->id);?>" role="button"><?php _e('+ Details', 'wpshards');?></a>
                </div>
                <?php endforeach;?>
            </div>

    </section>
   
</div>


<?php 
  endwhile;
  endif;
  get_footer();
?>